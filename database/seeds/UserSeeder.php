<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $u = new \App\User();
        $u->name = "Admin";
        $u->email = "admin@email.com";
        $u->password = bcrypt("Password1");
        $u->isAdmin = true;
        $u->save();

        // User / hacker
        $u = new \App\User();
        $u->name = "Hacker";
        $u->email = "hacker@email.com";
        $u->password = bcrypt("Password1");
        $u->isAdmin = false;
        $u->save();
    }
}
