<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Information
Ce projet démontre l'importante de la protection CSRF grâce à une page spécialement
forgée pour donner des droits à un utilisateur *hacker*. Le pirate ne peut pas lui-même
gagner des accès. Cependant, si l'administrateur clique sur un lien spécialement forgé,
celui-ci donnera des privilèges au *hacker* sans le vouloir.

## Prérequis
- Laravel 7.12.0
- sqlite configuré avec PHP ou MySQL (vérifier les fichiers [.env](https://gitlab.com/sebastien.huneault/demo-hack-csrf/-/blob/master/.env) et [.env.example](https://gitlab.com/sebastien.huneault/demo-hack-csrf/-/blob/master/.env.example))

## Commandes utiles
Installer les prérequis : `composer install`  
Lancer le site : `php artisan serve`  
Réinitialiser la base de données : `php artisan migrate:fresh --seed`  

## Usagers créés
Administrateur : admin@email.com / Password1  
Hacker : hacker@email.com / Password1

## URL du démo
Méthode|URL|Description
---|---|---
{+ GET +}|/login|Page de connexion
{+ GET +}|/home|Dashboard - gestionnaire d'usagers (accessible par l'admin seulement)
{+ GET +}|/hack|Page pour lancer le piratage (accessible dans le menu une fois connecté)
{- POST -}|/user/{id}|Modifier un usager (protégé par CSRF)
{- POST -}|/hack/user/{id}|Modifier un usager (non protégé par CSRF)

## Protection utilisée
- Une *gate* nommée `update-user` a été créée afin de protéger le code disponible
aux administrateurs seulement.
- Les routes sont protégées contre les invités - seuls des usagers connectés
ont accès au site.
- Un champ `isAdmin` est dans la base de données pour indiquer l'accès donné.

