@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @can('update-user')
                <div class="alert alert-success">
                    Vous êtes ADMIN
                </div>
                @foreach($users as $user)
                    <div class="card">
                        <div class="card-header">{{ $user->name }}</div>
                        <div class="card-body">
                            <form method="post" action="{{ route('user', $user) }}">
                                @csrf
                                <div class="form-group">
                                    <label for="name{{ $user->id }}">Nom</label>
                                    <input type="text" class="form-control" name="name" id="name{{ $user->id }}" value="{{ $user->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="email{{ $user->id }}">Courriel</label>
                                    <input type="email" class="form-control" name="email" id="email{{ $user->id }}" value="{{ $user->email }}">
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="isAdmin" id="chkIsAdmin{{ $user->id }}" {{ $user->isAdmin ? 'checked' : '' }} value="1">
                                    <label for="chkIsAdmin{{ $user->id }}" class="form-check-label">Est Admin</label>
                                </div>
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </form>
                        </div>
                    </div>
                    <br />
                @endforeach
            @else
                <div class="alert alert-danger">
                    Vous n'êtes pas ADMIN
                </div>
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Bienvenue {{ Request::User()->name  }}
                    </div>
                </div>
            @endcan
        </div>
    </div>
</div>
@endsection
