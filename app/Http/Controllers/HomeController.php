<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        return view('home', [
            'users' => $users
        ]);
    }

    public function updateUser(Request $request, User $user)
    {
        if (Gate::allows('update-user')) {
            $user->name = $request->post('name');
            $user->email = $request->post('email');
            $user->isAdmin = $request->post('isAdmin') == 1;
            $user->update();
        }
        return redirect('/home');
    }
}
