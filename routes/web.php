<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

// Mettre à jour l'utilisateur
Route::post('/user/{user}', 'HomeController@updateUser')->name('user')->middleware('auth');
Route::post('/hack/user/{user}', 'HomeController@updateUser')->name('hack-user')->middleware('auth');

// La route malveillante
Route::get('/hack', function () {
    return view('hack');
})->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
